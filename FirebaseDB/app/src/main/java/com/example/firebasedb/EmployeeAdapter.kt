package com.example.firebasedb

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.item.view.*
import kotlinx.android.synthetic.main.update.view.*

class EmployeeAdapter(val mcontext: Context, val resid: Int, val mirafraEmpList: List<MirafraEmp>) :
    ArrayAdapter<MirafraEmp>(mcontext, resid, mirafraEmpList) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layoutInflater: LayoutInflater = LayoutInflater.from(mcontext)
        val view: View = layoutInflater.inflate(resid, null)
        val list = mirafraEmpList[position]
        view.textView.text = list.name
        view.textViewUpdate.setOnClickListener {
            updaeDialog(list)
        }
        return view
    }

    fun updaeDialog(list: MirafraEmp) {
        val builder = AlertDialog.Builder(mcontext)
        builder.setTitle("update emp")
        val layoutInflater: LayoutInflater = LayoutInflater.from(mcontext)
        val view: View = layoutInflater.inflate(R.layout.update, null)
        view.editText.setText(list.name)
        view.ratingBar.rating= list.rating
        builder.setPositiveButton("update"){po,p1->
            var reference = FirebaseDatabase.getInstance().getReference("Employee")
            val name = view.editText.text.toString()
            val emp = MirafraEmp(list.id,name,view.ratingBar.rating)
            //for update
            reference.child(list.id.toString()).setValue(emp)
            //for delete
           // reference.child(list.id.toString()).removeValue()
            Toast.makeText(mcontext,"Value updated",Toast.LENGTH_LONG).show()
        }
        builder.setNegativeButton("cancel"){p0,p1->

        }
        builder.setView(view)
        val alert = builder.create()
        alert.show()

    }
}