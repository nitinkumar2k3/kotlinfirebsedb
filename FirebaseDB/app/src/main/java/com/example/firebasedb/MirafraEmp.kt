package com.example.firebasedb

class MirafraEmp(val id: String?, val name: String, val rating: Float){

    constructor(): this("","", 0.0F){}
}
