package com.example.firebasedb

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var reference: DatabaseReference
    lateinit var empList : MutableList<MirafraEmp>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        empList = mutableListOf()

        reference = FirebaseDatabase.getInstance().getReference("Employee")
        button.setOnClickListener {
            saveData()
        }

        // to read the value of firebase through interface
        reference.addValueEventListener(object : ValueEventListener{
            //error
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            // reading DataSnapshot is object of Employee
            override fun onDataChange(p0: DataSnapshot) {
                if(p0.exists()){
                    empList.clear()
                    for (emp in p0.children){
                        val emp = emp.getValue(MirafraEmp::class.java)
                        if (emp != null) {
                            empList.add(emp)
                        }
                    }
                    val adapter = EmployeeAdapter(this@MainActivity,R.layout.item,empList)
                    list_item.adapter = adapter
                }
            }

        })
    }

    private fun saveData() {

        if (editText.text.toString().trim().isEmpty()) {
            editText.error = "Please enter the name"
            return
        }

        val empID = reference.push().key
        val mirafraEmp = MirafraEmp(empID, editText.text.toString(), ratingBar.rating)
        reference.child(empID.toString()).setValue(mirafraEmp).addOnCompleteListener {
            Toast.makeText(this, "Data Sucessfully submmit", Toast.LENGTH_LONG).show()
        }

    }
}
